/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Kotlin Coroutines_

  https://commonsware.com/Coroutines
*/

package com.commonsware.jetpack.weather

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class MainMotor(application: Application) : AndroidViewModel(application) {
  private val _results = MutableStateFlow<MainViewState>(MainViewState.Loading)
  val results: Flow<MainViewState> = _results

  fun load(office: String, gridX: Int, gridY: Int) {
    val scenario = Scenario(office, gridX, gridY)
    val current = _results.value

    if (current !is MainViewState.Content || current.scenario != scenario) {
      _results.value = MainViewState.Loading

      viewModelScope.launch(Dispatchers.Main) {
        val result = WeatherRepository.load(office, gridX, gridY)

        _results.value = when (result) {
          is WeatherResult.Content -> {
            val rows = result.forecasts.map { forecast ->
              val temp = getApplication<Application>()
                .getString(
                  R.string.temp,
                  forecast.temperature,
                  forecast.temperatureUnit
                )

              RowState(forecast.name, temp, forecast.icon)
            }

            MainViewState.Content(scenario, rows)
          }
          is WeatherResult.Error -> MainViewState.Error(result.throwable)
        }
      }
    }
  }
}
