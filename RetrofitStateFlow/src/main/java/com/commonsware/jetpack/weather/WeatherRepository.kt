/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Kotlin Coroutines_

  https://commonsware.com/Coroutines
*/

package com.commonsware.jetpack.weather

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object WeatherRepository {
  private val api = Retrofit.Builder()
    .baseUrl("https://api.weather.gov")
    .addConverterFactory(MoshiConverterFactory.create())
    .build()
    .create(NWSInterface::class.java)

  suspend fun load(office: String, gridX: Int, gridY: Int) = try {
    val response = api.getForecast(office, gridX, gridY)

    WeatherResult.Content(response.properties?.periods ?: listOf())
  } catch (t: Throwable) {
    WeatherResult.Error(t)
  }
}
