/*
  Copyright (c) 2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Kotlin Coroutines_

  https://commonsware.com/Coroutines
*/

package com.commonsware.coroutines.misc

import androidx.lifecycle.asLiveData
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import kotlin.math.max
import kotlin.random.Random

private const val SEED = 1337

class LiveDataExamples {
  private val random = Random(SEED)

  fun liveSuspend() = liveData { emit(randomInt()) }

  private suspend fun randomInt(): Int = withContext(Dispatchers.IO) {
    delay(2000)

    max(1, random.nextInt())
  }

  fun liveFlow() = randomPercentages(3, 500).asLiveData(Dispatchers.IO)

  private fun randomPercentages(count: Int, delayMs: Long) = flow {
    for (i in 0 until count) {
      delay(delayMs)
      emit(random.nextInt(1, 100))
    }
  }
}